(function ($, Drupal, drupalSettings) {
    var selectedTypes = Object.values(drupalSettings.selected_types);
    var headers = $('th').slice(2).toArray();
    headers.forEach( h => {
        if ( !selectedTypes.includes(h.innerText) ) {
            h.classList.add("unselected");
        }
    })
})(jQuery, Drupal, drupalSettings);
