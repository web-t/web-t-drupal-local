<?php

namespace Drupal\webt;

/**
 * Provides an interface for WEB-T constants.
 */
interface WebtConstInterface {
  /**
   * Logger name.
   */
  const WEBT_LOGGER = 'webt';
  /**
   * Field which marks entity for translation update.
   */
  const WEBT_ENTITY_UPDATE = 'webt_update_entity';

}
