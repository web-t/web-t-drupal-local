<?php

namespace Drupal\webt\translation_engines\etranslation;

/**
 * Utility class for eTranslation provider.
 */
class EtranslationUtils {

  /**
   * Value used to mark request ended with eTranslation timeout.
   *
   * @var string
   */
  public static $timeoutValue = '_timeout';
  /**
   * Value used to determine that async translation request ended with error.
   *
   * @var string
   */
  public static $errorValue = '_error';
  /**
   * Default time in seconds to wait for async translation response.
   *
   * @var int
   */
  public static $etranslationTimeoutDefault = 10;

  /**
   * Encrypts user entered eTranslation password before it is stored in config.
   *
   * @param string $plaintext
   *   Plain text password.
   *
   * @return string
   *   Encrypted password
   */
  public static function encryptPassword($plaintext) {
    $host   = \Drupal::request()->getHost();
    $method = 'AES-256-CBC';
    $key    = hash('sha256', $host, TRUE);
    $iv     = openssl_random_pseudo_bytes(16);

    $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
    $hash       = hash_hmac('sha256', $ciphertext . $iv, $key, TRUE);

    return base64_encode($iv . $hash . $ciphertext);
  }

  /**
   * Decrypts eTranslation password stored in config.
   *
   * @param string $base64cipher
   *   Encrypted password string.
   *
   * @return string
   *   Plaintext password
   */
  public static function decryptPassword($base64cipher) {
    $host             = \Drupal::request()->getHost();
    $method           = 'AES-256-CBC';
    $ivHashCiphertext = base64_decode($base64cipher);
    $iv               = substr($ivHashCiphertext, 0, 16);
    $hash             = substr($ivHashCiphertext, 16, 32);
    $ciphertext       = substr($ivHashCiphertext, 48);
    $key              = hash('sha256', $host, TRUE);

    if (!hash_equals(hash_hmac('sha256', $ciphertext . $iv, $key, TRUE), $hash)) {
      return NULL;
    }

    return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
  }

  /**
   * Generates cache key for translation request.
   *
   * @param string $request_id
   *   Request ID.
   *
   * @return string
   *   Cache identifier key
   */
  public static function getCacheKey($request_id) {
    return "etranslation/$request_id";
  }

}
