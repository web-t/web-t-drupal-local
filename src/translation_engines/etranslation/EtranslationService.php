<?php

namespace Drupal\webt\translation_engines\etranslation;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Url;
use Drupal\webt\translation_engines\AbstractTranslationService;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Psr7\Response;

/**
 * Machine translation service implementation for eTranslation provider.
 */
class EtranslationService extends AbstractTranslationService {

  /**
   * Cache.
   *
   * @var Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Char limit when to split translatable strings into separate request.
   *
   * @var int
   */
  public static $defaultCharsPerRequest = 60000;

  /**
   * Error response code dictionary.
   *
   * @var array
   */
  private static $errorMap = [
    -20000 => 'Source language not specified',
    -20001 => 'Invalid source language',
    -20002 => 'Target language(s) not specified',
    -20003 => 'Invalid target language(s)',
    -20004 => 'DEPRECATED',
    -20005 => 'Caller information not specified',
    -20006 => 'Missing application name',
    -20007 => 'Application not authorized to access the service',
    -20008 => 'Bad format for ftp address',
    -20009 => 'Bad format for sftp address',
    -20010 => 'Bad format for http address',
    -20011 => 'Bad format for email address',
    -20012 => 'Translation request must be text type, document path type or document base64 type and not several at a time',
    -20013 => 'Language pair not supported by the domain',
    -20014 => 'Username parameter not specified',
    -20015 => 'Extension invalid compared to the MIME type',
    -20016 => 'DEPRECATED',
    -20017 => 'Username parameter too long',
    -20018 => 'Invalid output format',
    -20019 => 'Institution parameter too long',
    -20020 => 'Department number too long',
    -20021 => 'Text to translate too long',
    -20022 => 'Too many FTP destinations',
    -20023 => 'Too many SFTP destinations',
    -20024 => 'Too many HTTP destinations',
    -20025 => 'Missing destination',
    -20026 => 'Bad requester callback protocol',
    -20027 => 'Bad error callback protocol',
    -20028 => 'Concurrency quota exceeded',
    -20029 => 'Document format not supported',
    -20030 => 'Text to translate is empty',
    -20031 => 'Missing text or document to translate',
    -20032 => 'Email address too long',
    -20033 => 'Cannot read stream',
    -20034 => 'Output format not supported',
    -20035 => 'Email destination tag is missing or empty',
    -20036 => 'HTTP destination tag is missing or empty',
    -20037 => 'FTP destination tag is missing or empty',
    -20038 => 'SFTP destination tag is missing or empty',
    -20039 => 'Document to translate tag is missing or empty',
    -20040 => 'Format tag is missing or empty',
    -20041 => 'The content is missing or empty',
    -20042 => 'Source language defined in TMX file differs from request',
    -20043 => 'Source language defined in XLIFF file differs from request',
    -20044 => 'Output format is not available when quality estimate is requested. It should be blank or \'xslx\'',
    -20045 => 'Quality estimate is not available for text snippet',
    -20046 => 'Document too big (>20Mb)',
    -20047 => 'Quality estimation not available',
    -40010 => 'Too many segments to translate',
    -80004 => 'Cannot store notification file at specified FTP address',
    -80005 => 'Cannot store notification file at specified SFTP address',
    -80006 => 'Cannot store translated file at specified FTP address',
    -80007 => 'Cannot store translated file at specified SFTP address',
    -90000 => 'Cannot connect to FTP',
    -90001 => 'Cannot retrieve file at specified FTP address',
    -90002 => 'File not found at specified address on FTP',
    -90007 => 'Malformed FTP address',
    -90012 => 'Cannot retrieve file content on SFTP',
    -90013 => 'Cannot connect to SFTP',
    -90014 => 'Cannot store file at specified FTP address',
    -90015 => 'Cannot retrieve file content on SFTP',
    -90016 => 'Cannot retrieve file at specified SFTP address',
  ];

  /**
   * Service API URL.
   *
   * @var string
   */
  private static $apiUrl = 'https://webgate.ec.europa.eu/etranslation/si';

  /**
   * Do not retry if response has not been received within specified timeout.
   *
   * @var bool
   */
  protected $retryOnEntityTranslationError = FALSE;

  /**
   * HTTP client request timeout.
   *
   * @var int
   */
  private $requestTimeout = 600;
  /**
   * Async translation response wait timeout.
   *
   * @var int
   */
  private $etranslationTimeout;

  /**
   * XML tag which precedes every string translation.
   *
   * @var string
   */
  private $segmentOpening = '<segment>';
  /**
   * XML tag which closes every string translation.
   *
   * @var string
   */
  private $segmentClosing = '</segment>';
  /**
   * XML tag part between translations.
   *
   * @var string
   */
  private $delimiter;

  /**
   * Set service parameters from config and initialize service.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   HTTP client factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   */
  public function __construct(ClientFactory $http_client_factory, $cache) {
    parent::__construct($http_client_factory);
    $this->httpClient          = $http_client_factory->fromOptions(
      ['timeout' => $this->requestTimeout, 'verify' => FALSE]
    );
    $this->cache               = $cache;
    $this->etranslationTimeout = $this->config->get('etranslation_timeout') ? $this->config->get('etranslation_timeout') : EtranslationUtils::$etranslationTimeoutDefault;
    $this->maxCharsPerRequest  = $this->config->get('etranslation_chars_per_request') ? $this->config->get('etranslation_chars_per_request') : self::$defaultCharsPerRequest;
    $this->delimiter           = $this->segmentClosing . $this->segmentOpening;
  }

  /**
   * Translate given XML strings via eTranslation.
   *
   * @param string $from
   *   Source langcode.
   * @param string $to
   *   Target langcode.
   * @param string[] $xml_values
   *   Translatable XML strings.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string[]
   *   Translations
   */
  protected function sendTranslationRequest($from, $to, $xml_values, $entity = NULL) {

    if (!$this->isAuthorized()) {
      return [];
    }

    // Prepare translation request.
    $id         = uniqid();
    $xml_prefix = '<?xml version="1.0" encoding="utf-8" standalone="yes" ?><root>' . $this->segmentOpening;
    $xml_suffix = $this->segmentClosing . '</root>';
    $content    = $xml_prefix . implode($this->delimiter, $xml_values) . $xml_suffix;

    if (strlen($content) === 0) {
      return [];
    }

    $post = $this->getPostBody($id, $from, $to, $content, $entity);

    $options = [
      'headers' => [
        'Content-Type'   => 'application/json',
        'Content-Length' => strlen($post),
      ],
      'body'    => $post,
      'auth'    => [$this->getApplicationName(), $this->getPassword(), 'digest'],
    ];

    try {
      // Send translation request.
      $response = $this->httpClient->post(self::$apiUrl . '/translate', $options);
      // Check request response.
      $body       = (string) $response->getBody();
      $request_id = is_numeric($body) ? (int) $body : NULL;
      $status     = $response->getStatusCode();
      if (200 !== $status || $request_id < 0) {
        $message = self::$errorMap[$request_id] ?? $body;
        $this->logger->error("Invalid request response from eTranslation: @message [status: @status]",
          [
            '@status' => $status,
            '@message' => $message,
          ]
        );
        return [];
      }

      $this->logger->info("eTranslation request successful (@request_id)", ['@request_id' => $request_id]);

      // Wait for translation callback.
      $translations = $this->awaitEtranslationResponse($request_id, NULL === $entity);
      if ($translations) {
        return $this->etranslationResponseToTranslationArray($translations->data);
      }
      else {
        return [];
      }
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $message  = $response ? (string) $response->getBody() : '';
      if ($response && strlen($message) == 0) {
        $message = Message::toString($response);
      }
      $this->logger->error('Error translating strings: @message', ['@message' => $message]);
      return [];
    }
    catch (ConnectException $e) {
      $this->logger->error('Could not establish connection with eTranslation. Error: @message', ['@message' => $e->getMessage()]);
      return [];
    }
  }

  /**
   * Split translated string response to separate XML strings.
   *
   * @param string $response
   *   Response body.
   *
   * @return string[]
   *   Translations
   */
  public function etranslationResponseToTranslationArray($response) {
    // Extract translations.
    $segment_tag_length = strlen($this->segmentOpening);
    $first_segment_pos  = strpos($response, $this->segmentOpening);
    $data_start_pos     = $first_segment_pos + $segment_tag_length;
    $data_end_pos       = strrpos($response, $this->segmentClosing);
    $segment_string     = substr($response, $data_start_pos, $data_end_pos - $data_start_pos);

    $translations = explode($this->delimiter, $segment_string);
    return $translations;
  }

  /**
   * Builds translation request body.
   *
   * @param string $id
   *   Translation request ID.
   * @param string $lang_from
   *   Source language.
   * @param string $lang_to
   *   Target language.
   * @param string $translatable_string
   *   Translatable text.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string
   *   JSON body
   */
  private function getPostBody($id, $lang_from, $lang_to, $translatable_string, $entity = NULL) {
    $site_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    $document = [
      'content'  => base64_encode($translatable_string),
      'format'   => 'xml',
      'filename' => 'translateMe',
    ];

    $domain = $this->config->get("mt_domain_$lang_to");

    $translation_request_body = [
      'documentToTranslateBase64' => $document,
      'sourceLanguage'            => strtoupper($lang_from),
      'targetLanguages'           => [
        strtoupper($lang_to),
      ],
      'errorCallback'             => $site_url . '/etranslation/error/' . $id,
      'callerInformation'         => [
        'application' => $this->getApplicationName(),
      ],
      'destinations'              => [
        'httpDestinations' => [
          $site_url . '/etranslation/translation/' . $id,
        ],
      ],
      'domain'                    => $domain ? $domain : 'GEN',
    ];
    if ($entity) {
      $translation_request_body['externalReference'] = $entity->id() . ':' . $entity->getEntityTypeId();
    }
    return json_encode($translation_request_body, JSON_UNESCAPED_SLASHES);
  }

  /**
   * Wait for async translation response and return its body.
   *
   * @param string $request_id
   *   Request ID.
   * @param bool $is_pretranslation
   *   Whether this request is part of pre-translation process.
   *
   * @return string
   *   Response
   */
  private function awaitEtranslationResponse($request_id, $is_pretranslation = TRUE) {
    $response          = NULL;
    $key               = EtranslationUtils::getCacheKey($request_id);
    $start_time        = microtime(TRUE);
    $check_interval_ms = 250;

    $timeout = $is_pretranslation ? $this->requestTimeout : $this->etranslationTimeout;

    while (!$response && $timeout > 0 && microtime(TRUE) - $start_time < $timeout) {
      $response = $this->cache->get($key);
      usleep($check_interval_ms * 1000);
    }
    if (!$response) {
      $this->logger->info("eTranslation response timeout (@request_id, @timeout s)",
        [
          '@request_id' => $request_id,
          '@timeout' => $timeout,
        ]
      );
      $this->cache->set($key, EtranslationUtils::$timeoutValue);
      return NULL;
    }
    else {
      $this->cache->delete($key);
      if (EtranslationUtils::$errorValue === $response->data) {
        return NULL;
      }
      return $response;
    }
  }

  /**
   * Send language direction (domain) request.
   *
   * @return Psr\Http\Message\ResponseInterface
   *   Response
   */
  protected function sendLanguageDirectionRequest() {
    $options = [
      'auth' => [$this->getApplicationName(), $this->getPassword(), 'digest'],
    ];

    try {
      return $this->httpClient->get(self::$apiUrl . '/get-domains', $options);
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $message  = $response ? (string) $response->getBody() : '';
      if ($response && strlen($message) == 0) {
        $message = Message::toString($response);
      }
      $this->logger->error('Error on language direction request: @message', ['@message' => $message]);
      return $response;
    }
    catch (ConnectException $e) {
      $this->logger->error('Could not establish connection with eTranslation. Error: @message', ['@message' => $e->getMessage()]);
      return new Response(408);
    }
  }

  /**
   * Checks if MT provider data is configured.
   *
   * @return bool
   *   True if eTranslation credentials are configure, false otherwise
   */
  public function isAuthorized() {
    if (!$this->getApplicationName() || !$this->getPassword()) {
      $this->logger->error('eTranslation credentials not configured. Please go to Translation provider settings and enter MT authorization data!');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Map language direction request response to Generic API JSON response.
   *
   * @param string $body
   *   Response body.
   *
   * @return string
   *   JSON body
   */
  protected function mapLanguageDirectionResponseBody($body) {
    $systems = [];
    foreach (json_decode($body) as $domain => $value) {
      $lang_pairs = $value->languagePairs;
      foreach ($lang_pairs as $pair) {
        $lower = strtolower($pair);
        $langs = explode('-', $lower);

        $system          = new \stdClass();
        $system->srcLang = $langs[0];
        $system->trgLang = $langs[1];
        $system->domain  = $domain;
        $system->name    = $value->name;
        $systems[]       = $system;
      }
    }
    $response                     = new \stdClass();
    $response->languageDirections = $systems;
    return json_encode($response);
  }

  /**
   * Retrieves user specified eTranslation application name from configuration.
   *
   * @return string
   *   eTranslation application/user name
   */
  private function getApplicationName() {
    return $this->config->get('etranslation_appname');
  }

  /**
   * Retrieves and decrypts eTranslation password from configuration.
   *
   * @return string
   *   eTranslation API password or null if password not found
   */
  private function getPassword() {
    $encrypted = $this->config->get('etranslation_password');
    if ($encrypted) {
      return EtranslationUtils::decryptPassword($encrypted);
    }
    return NULL;
  }

}
