<?php

namespace Drupal\webt\translation_engines\generic;

use Drupal\Core\Http\ClientFactory;
use Drupal\webt\translation_engines\AbstractTranslationService;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Psr7\Response;

/**
 * Machine translation service implementation for Custom provider.
 */
class GenericMTService extends AbstractTranslationService {

  /**
   * Char limit when to split translatable strings into separate request.
   *
   * @var int
   */
  public static $defaultCharsPerRequest = 1500;

  /**
   * Set service parameters from config and initialize service.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   HTTP client factory.
   */
  public function __construct(ClientFactory $http_client_factory) {
    parent::__construct($http_client_factory);
    $this->maxCharsPerRequest = $this->config->get('mt_chars_per_request') ? $this->config->get('mt_chars_per_request') : self::$defaultCharsPerRequest;
  }

  /**
   * Translate given XML strings via Custom provider.
   *
   * @param string $from
   *   Source langcode.
   * @param string $to
   *   Target langcode.
   * @param string[] $xml_values
   *   Translatable XML strings.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string[]
   *   Translations
   */
  protected function sendTranslationRequest($from, $to, $xml_values, $entity = NULL) {
    $data          = (object) [];
    $data->srcLang = $from;
    $data->trgLang = $to;
    $data->text    = $xml_values;

    $api_url = $this->config->get('mt_api_url');
    $api_key = $this->config->get('mt_api_key');
    $domain  = $this->config->get("mt_domain_$to");
    if ($domain) {
      $data->domain = $domain;
    }

    if (!$this->isAuthorized()) {
      return [];
    }

    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'accept'       => 'text/plain',
        'X-API-KEY'    => $api_key,
      ],
      'body'    => json_encode($data, JSON_UNESCAPED_SLASHES),
    ];
    try {
      $response = $this->httpClient->post($api_url . '/translate/text', $options);
      $json     = json_decode($response->getBody());

      $translations = array_map(
      function ($item) {
        return stripslashes($item->translation);
      },
                $json->translations
      );
      return $translations;
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $message  = $response ? (string) $response->getBody() : '';
      if ($response && strlen($message) == 0) {
        $message = Message::toString($response);
      }
      $this->logger->error('Error translating strings: @message', ['@message' => $message]);
      return [];
    }
    catch (ConnectException $e) {
      $this->logger->error('Could not establish connection with the MT provider. Check your API URL or try again later! Error: @message', ['@message' => $e->getMessage()]);
      return [];
    }
  }

  /**
   * Send language direction (domain) request.
   *
   * @return Psr\Http\Message\ResponseInterface
   *   Response
   */
  protected function sendLanguageDirectionRequest() {
    $api_url = $this->config->get('mt_api_url');
    $api_key = $this->config->get('mt_api_key');

    $options = [
      'headers' => [
        'accept'    => 'text/plain',
        'X-API-KEY' => $api_key,
      ],
    ];

    try {
      return $this->httpClient->get($api_url . '/translate/language-directions', $options);
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $message  = $response ? (string) $response->getBody() : '';
      if ($response && strlen($message) == 0) {
        $message = Message::toString($response);
      }
      $this->logger->error('Error on language direction request: @message', ['@message' => $message]);
      return $response;
    }
    catch (ConnectException $e) {
      $this->logger->error('Could not establish connection with the MT provider. Check your API URL or try again later! Error: @message', ['@message' => $e->getMessage()]);
      return new Response(408);
    }
  }

  /**
   * Checks if MT API URL & key is provided.
   *
   * @return bool
   *   True if MT API data is set, false otherwise
   */
  public function isAuthorized() {
    $api_url = $this->config->get('mt_api_url');
    $api_key = $this->config->get('mt_api_key');
    if (!$api_url || !$api_key) {
      $this->logger->error('MT API URL or key not set. Please go to Translation provider settings and enter MT authorization data!');
      return FALSE;
    }
    return TRUE;
  }

}
