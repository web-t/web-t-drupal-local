<?php

namespace Drupal\webt\Model;

/**
 * Translation response object.
 */
class TranslationResponse {
  /**
   * Translation status.
   *
   * @var int
   */
  public $status;
  /**
   * String translations.
   *
   * @var string[]
   */
  public $translations;

  /**
   * Sets response translations and status.
   *
   * @param string[] $translations
   *   Translations.
   * @param int $status
   *   Status.
   */
  public function __construct($translations, $status = TranslationStatus::FULL_RESULT) {
    $this->translations = $translations;
    $this->status       = $status;
  }

}
