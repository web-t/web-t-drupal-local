<?php

namespace Drupal\webt\Model;

/**
 * Response statuses for any processing jobs.
 */
class ProcessingResponse {
  /**
   * Processing error.
   *
   * @var int
   */
  public static $processingError = 0;
  /**
   * Partially successful processing.
   *
   * @var int
   */
  public static $partiallyProcessed = 1;
  /**
   * Successful processing.
   *
   * @var int
   */
  public static $fullyProcessed = 2;
  /**
   * Processing not needed.
   *
   * @var int
   */
  public static $alreadyProcessed = 3;

}
