<?php

namespace Drupal\webt\Model;

/**
 * Translatable string types.
 */
class StringType {
  const NODE    = 'node';
  const COMMENT = 'comment';
  const TAG     = 'taxonomy_term';
  const CONFIG  = 'configuration';
  const UI      = 'ui';

  /**
   * Retrieves all string types listed above.
   *
   * @return array
   *   StringType list
   */
  public static function getAllTypes() {
    $oClass = new \ReflectionClass(__CLASS__);
    return $oClass->getConstants();
  }

  /**
   * Retrieves localized display name for given string type.
   *
   * @param string $type
   *   String type.
   *
   * @return string
   *   Localized StringType names
   */
  public static function getDisplayName($type) {
    switch ($type) {
      case self::NODE:
        return t('Nodes');

      case self::COMMENT:
        return t('Comments');

      case self::TAG:
        return t('Tags');

      case self::CONFIG:
        return t('Configuration');

      case self::UI:
        return t('UI elements');
    }
  }

}
