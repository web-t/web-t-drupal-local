<?php

namespace Drupal\webt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webt\translation_engines\etranslation\EtranslationService;
use Drupal\webt\translation_engines\etranslation\EtranslationUtils;
use Drupal\webt\translation_engines\generic\GenericMTService;
use Drupal\webt\WebtConstInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Machine translation provider setting form.
 */
class TranslationProviderForm extends ConfigFormBase {

  /**
   * Internal id of eTranslation provider.
   *
   * @var string
   */
  private $etranslationEngineId = 'etranslation';
  /**
   * Internal id of Custom provider.
   *
   * @var string
   */
  private $genericMtEngineId = 'generic';

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Translation manager service.
   *
   * @var \Drupal\webt\TranslationManager
   */
  protected $translationManager;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * URL generator.
   *
   * @var Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $translation_manager
   *   The translation manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   URL generator.
   */
  public function __construct(
      $config_factory,
      $translation_manager,
      $language_manager,
      $urlGenerator
    ) {
    $this->logger             = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
    $this->configFactory      = $config_factory;
    $this->translationManager = $translation_manager;
    $this->languageManager    = $language_manager;
    $this->urlGenerator       = $urlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('webt.translation_manager'),
      $container->get('language_manager'),
      $container->get('url_generator')
    );
  }

  /**
   * Retrieve Drupal form id.
   *
   * @return string
   *   Form ID
   */
  public function getFormId() {
    return 'TranslationProviderForm';
  }

  /**
   * Adds fields and libraries to the form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return Drupal\Core\Form\FormInterface
   *   Form definition
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webt.settings');

    $selected_mt_engine = $config->get('mt_engine');

    // Conditions of current provider radio choice value.
    $condition_etranslation = [
      ':input[name="mt_engine"]' => ['value' => $this->etranslationEngineId],
    ];

    $condition_generic_mt = [
      ':input[name="mt_engine"]' => ['value' => $this->genericMtEngineId],
    ];

    $condition_selected_engine = [
      ':input[name="mt_engine"]' => ['value' => $selected_mt_engine],
    ];

    // Add authorization section.
    $form['authorization'] = [
      '#type'                 => 'fieldset',
      '#title'                => $this->t('Translation provider settings'),
      '#collapsible'          => FALSE,
      '#collapsed'            => FALSE,
      'description'           => [
        '#type'   => 'markup',
        '#markup' => '<div class="form-item__description">' . $this->t('Choose machine translation provider to use in website translation.') . '</div>',
      ],
      'mt_engine'             => [
        '#type'          => 'radios',
        '#title'         => $this->t('Translation provider'),
        '#options'       => [
          $this->etranslationEngineId => $this->t('eTranslation'),
          $this->genericMtEngineId   => $this->t('Custom provider'),
        ],
        '#default_value' => $selected_mt_engine ? $selected_mt_engine : $this->etranslationEngineId,
      ],
      'etranslation_appname'  => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Application name'),
        '#default_value' => $config->get('etranslation_appname'),
        '#states'        => [
          'visible'  => $condition_etranslation,
          'required' => $condition_etranslation,
          'disabled' => $condition_generic_mt,
        ],
      ],
      'etranslation_password' => [
        '#type'       => 'password',
        '#title'      => $this->t('Password'),
        '#attributes' => ['value' => $config->get('etranslation_password')],
        '#states'     => [
          'visible'  => $condition_etranslation,
          'required' => $condition_etranslation,
          'disabled' => $condition_generic_mt,
        ],
      ],
      'etranslation_help'     => [
        '#type'       => 'container',
        '#attributes' => ['class' => ['form-item__description']],
        '#states'     => [
          'visible' => $condition_etranslation,
        ],
        'help_text'   => [
          '#type'   => 'markup',
          '#markup' => $this->t('Set up an account for <a target="_blank" href=":link">eTranslation</a>',
            [
              ':link' => 'https://website-translation.language-tools.ec.europa.eu/automated-translation_en',
            ]
          ),
        ],
      ],
      'mt_api_url'            => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Base URL'),
        '#default_value' => $config->get('mt_api_url'),
        '#placeholder'   => 'https://example.com/api',
        '#states'        => [
          'visible'  => $condition_generic_mt,
          'required' => $condition_generic_mt,
          'disabled' => $condition_etranslation,
        ],
      ],
      'mt_api_key'            => [
        '#type'          => 'textfield',
        '#title'         => $this->t('API key'),
        '#default_value' => $config->get('mt_api_key'),
        '#states'        => [
          'visible'  => $condition_generic_mt,
          'required' => $condition_generic_mt,
          'disabled' => $condition_etranslation,
        ],
      ],
      'mt_api_help'           => [
        '#type'       => 'container',
        '#attributes' => ['class' => ['form-item__description']],
        '#states'     => [
          'visible' => $condition_generic_mt,
        ],
        'help_text'   => [
          '#type'   => 'markup',
          '#markup' => $this->t('Get <a target="_blank" href=":link">MT provider access</a>',
            [
              ':link' => 'https://website-translation.language-tools.ec.europa.eu/automated-translation_en',
            ]
          ),
        ],
      ],
      'button_wrapper'        => [
        '#type'       => 'container',
        '#attributes' => ['class' => ['text-align-right']],
        'submit_auth' => [
          '#type'   => 'submit',
          '#value'  => $this->t('Save'),
          '#name'   => 'submit_auth',
          '#submit' => ['::submitForm'],
        ],
      ],
    ];

    // Add configuration section if auth data is saved.
    if ($this->machineTranslationConfigured()) {
      $form['configuration'] = [
        '#type'        => 'fieldset',
        '#title'       => $this->t('Configuration'),
        '#collapsible' => FALSE,
        '#collapsed'   => FALSE,
        'description'  => [
          '#type'   => 'markup',
          '#markup' => '<div class="form-item__description">' . $this->t('Select MT engines to use in translation and configure advanced settings (optional).') . '</div>',
        ],
        'mt_engines'   => [
          '#type'        => 'fieldset',
          '#title'       => $this->t('Machine translation engines'),
          '#collapsible' => FALSE,
          '#collapsed'   => FALSE,
          'fields'       => $this->getMtEngineFields(),
        ],
        '#states'      => [
          'visible' => $condition_selected_engine,
        ],
      ];

      $form['configuration']['etranslation_advanced'] = [
        '#type'                          => 'details',
        '#title'                         => $this->t('Advanced'),
        '#open'                          => FALSE,
        '#tree'                          => TRUE,
        '#states'                        => [
          'visible'  => $condition_etranslation,
          'required' => $condition_etranslation,
          'disabled' => $condition_generic_mt,
        ],
        'etranslation_timeout'           => [
          '#type'          => 'number',
          '#title'         => $this->t('eTranslation Timeout'),
          '#description'   => $this->t('Max time in seconds to wait for eTranslation to translate new/updated entities on save (per language). Infinite if zero. Does not affect pre-translation.'),
          '#step'          => '0.1',
          '#default_value' => $config->get('etranslation_timeout') ? $config->get('etranslation_timeout') : EtranslationUtils::$etranslationTimeoutDefault,
        ],
        'etranslation_chars_per_request' => [
          '#type'          => 'number',
          '#title'         => $this->t('Max characters per single request'),
          '#description'   => $this->t('Affects how many/big requests are sent to MT provider, which can influence translation speed.'),
          '#step'          => '100',
          '#default_value' => $config->get('etranslation_chars_per_request') ? $config->get('etranslation_chars_per_request') : EtranslationService::$defaultCharsPerRequest,
        ],
      ];

      $form['configuration']['mt_advanced'] = [
        '#type'                => 'details',
        '#title'               => $this->t('Advanced'),
        '#open'                => FALSE,
        '#tree'                => TRUE,
        '#states'              => [
          'visible'  => $condition_generic_mt,
          'required' => $condition_generic_mt,
          'disabled' => $condition_etranslation,
        ],
        'mt_chars_per_request' => [
          '#type'          => 'number',
          '#title'         => $this->t('Max characters per single request'),
          '#description'   => $this->t('Affects how many/big requests are sent to MT provider, which can influence translation speed.'),
          '#step'          => '100',
          '#default_value' => $config->get('mt_chars_per_request') ? $config->get('mt_chars_per_request') : GenericMTService::$defaultCharsPerRequest,
        ],
      ];

      $form['configuration']['button_wrapper'] = [
        '#type'         => 'container',
        '#attributes'   => ['class' => ['text-align-right']],
        'submit_config' => [
          '#type'   => 'submit',
          '#value'  => $this->t('Save'),
          '#name'   => 'submit_config',
          '#submit' => ['::submitConfig'],
        ],
      ];
    }

    $form['#attached']['library'][] = 'webt/setting_tab.style';

    return $form;
  }

  /**
   * Function executed on valid authorization setting save event.
   *
   * Saves submitted data in Drupal config.
   *
   * @param array $form
   *   Submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form's state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    $config    = $this->configFactory->getEditable('webt.settings');
    $mt_engine = $form_state->getValue('mt_engine');
    $config->set('mt_engine', $mt_engine);

    if ($this->etranslationEngineId === $mt_engine) {
      $config->set('etranslation_appname', $form_state->getValue('etranslation_appname'));
      if ($config->get('etranslation_password') !== $form_state->getValue('etranslation_password')) {
        $encrypted_pwd = EtranslationUtils::encryptPassword($form_state->getValue('etranslation_password'));
        $config->set('etranslation_password', $encrypted_pwd);
      }
    }
    elseif ($this->genericMtEngineId === $mt_engine) {
      $config->set('mt_api_url', $form_state->getValue('mt_api_url'));
      $config->set('mt_api_key', $form_state->getValue('mt_api_key'));
    }
    $config->clear('language_directions');

    $config->save();
    $this->messenger()->addStatus($this->t('Authorization settings saved.'));
    $this->checkMachineTranslatonConnection();
  }

  /**
   * Validation function executed on authorization setting save event.
   *
   * Currently checks if Custom provider URL is entered correctly.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $engine = $form_state->getValue('mt_engine');
    switch ($engine) {
      case $this->etranslationEngineId:
        return;

      case $this->genericMtEngineId:
        $url_key    = 'mt_api_url';
        $mt_api_url = $form_state->getValue($url_key);
        if (!filter_var($mt_api_url, FILTER_VALIDATE_URL)) {
          $form_state->setErrorByName($url_key, $this->t('Please enter a valid URL'));
        }
        break;

      default:
        $this->logger->error("Invalid translation engine: '@engine'. Please recheck your MT provider authorization settings!", ['@engine' => $engine]);
        return;
    }
  }

  /**
   * Function executed on valid configuration setting save event.
   *
   * Saves submitted data in Drupal config.
   *
   * @param array $form
   *   Submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form's state.
   */
  public function submitConfig(array &$form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    $config = $this->configFactory->getEditable('webt.settings');

    $mt_engine = $form_state->getValue('mt_engine');

    if ($this->etranslationEngineId === $mt_engine) {
      $config->set('etranslation_timeout', $form_state->getValue('etranslation_advanced')['etranslation_timeout']);
      $config->set('etranslation_chars_per_request', $form_state->getValue('etranslation_advanced')['etranslation_chars_per_request']);
    }
    elseif ($this->genericMtEngineId === $mt_engine) {
      $config->set('mt_chars_per_request', $form_state->getValue('mt_advanced')['mt_chars_per_request']);
    }
    $langcodes = array_keys($this->languageManager->getLanguages());
    foreach ($langcodes as $langcode) {
      $domain = $form_state->getValue("mt_domain_$langcode");
      $config->set("mt_domain_$langcode", $domain);
    }
    $config->save();
    $this->messenger()->addStatus($this->t('Provider configuration saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webt.settings',
    ];
  }

  /**
   * Checks if MT authorization data is provided for selected MT provider.
   *
   * @return bool
   *   True if MT data is provided, false otherwise.
   */
  private function machineTranslationConfigured() {
    $config = $this->config('webt.settings');
    $engine = $config->get('mt_engine');
    switch ($engine) {
      case $this->etranslationEngineId:
        return $config->get('etranslation_appname') && $config->get('etranslation_password');

      case $this->genericMtEngineId:
        return $config->get('mt_api_url') && $config->get('mt_api_key');

      default:
        return FALSE;
    }
  }

  /**
   * Checks if connection with machine translation service is successful.
   *
   * @return bool
   *   True if MT connection is successful, false otherwise.
   */
  public function checkMachineTranslatonConnection() {
    $translation_service   = $this->translationManager->translationService;
    $connection_successful = $translation_service->testConnection();
    if ($connection_successful) {
      $this->messenger()->addStatus($this->t('Machine-translation connection successful'));
      return TRUE;
    }
    else {
      $this->messenger()->addError($this->t('There was a problem connecting to machine-translation provider. Check your MT configuration, <a href=":url">logs</a> or try again later.', [':url' => $this->urlGenerator->generate('dblog.overview')]));
      return FALSE;
    }
  }

  /**
   * Creates dropdown field container for machine translation engine selection.
   *
   * Adds warning if language not supported by MT provider.
   *
   * @return array
   *   Container definition
   */
  private function getMtEngineFields() {
    $config                = $this->config('webt.settings');
    $translation_service   = $this->translationManager->translationService;
    $supported_languages   = $translation_service->getSupportedLanguages();
    $unsupported_languages = [];

    $langcodes = $this->languageManager->getLanguages();
    $selects   = [];
    foreach ($langcodes as $langcode => $value) {
      $directions = array_filter(
      $supported_languages,
      function ($direction) use ($langcode) {
        return $direction->trgLang === $langcode;
      }
      );
      $options = [NULL => $this->t('[Unspecified]')];
      foreach ($directions as $direction) {
        $options[$direction->domain] = $direction->name ?? $direction->domain;
      }
      $select                         = [
        '#title'         => $value->getName(),
        '#type'          => 'select',
        '#options'       => $options,
        '#default_value' => $config->get("mt_domain_$langcode"),
      ];
      $selects["mt_domain_$langcode"] = $select;

      if (1 === count($options)) {
        $unsupported_languages[] = $value->getName();
      }
    }
    if (count($unsupported_languages) > 0) {
      $list_str = implode(', ', $unsupported_languages);
      $this->messenger()->addWarning($this->t('Current MT provider does not support following languages: @languages!', ['@languages' => $list_str]));
    }
    return [
      '#type'       => 'container',
      'selects'     => $selects,
      '#attributes' => ['class' => 'mt_engines_container'],
    ];
  }

}
