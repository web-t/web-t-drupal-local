<?php

namespace Drupal\webt\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\webt\translation_engines\etranslation\EtranslationService;
use Drupal\webt\translation_engines\etranslation\EtranslationUtils;
use Drupal\webt\WebtConstInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that handles eTranslation async responses.
 */
class EtranslationCallbackController extends ControllerBase {
  /**
   * The translation manager service.
   *
   * @var \Drupal\webt\TranslationManager
   */
  protected $translationManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\webt\TranslationManager $translationManager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct($translationManager, $entityTypeManager) {
    $this->translationManager = $translationManager;
    $this->logger = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webt.translation_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Receive & process async translation response from eTranslation.
   *
   * If this is a translaton for an entity and timeout has been reached before,
   * also parse and save translation.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   HTTP request object.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Response
   */
  public function onResponse(Request $request) {
    $cache              = $this->cache();
    $request_id         = Html::escape($request->get('request-id'));
    $translated_content = base64_decode(file_get_contents('php://input'));
    $key                = EtranslationUtils::getCacheKey($request_id);
    $this->logger->info("Received response from eTranslation (@request_id)", ['@request_id' => $request_id]);
    if ($cache->get($key) && $cache->get($key)->data === EtranslationUtils::$timeoutValue) {
      $this->logger->info("Received late translation (@request_id)", ['@request_id' => $request_id]);
      $entityInfo = Html::escape($request->get('external-reference'));
      if ($entityInfo) {
        $entityInfoParts = explode(':', $entityInfo);
        $entity_id       = $entityInfoParts[0];
        $entity_type     = $entityInfoParts[1];
        $entity          = $this->translationManager->loadEntityByIdAndType($entity_id, $entity_type);
        if ($entity) {
          $this->processEntityTranslation($entity, $translated_content, Html::escape($request->get('target-language')));
        }
      }
    }
    else {
      $cache->set($key, $translated_content);
    }
    return new Response();
  }

  /**
   * Process async error response from eTranslation.
   *
   * Logs error and marks translation
   * as completed with error.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   HTTP request object.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Response
   */
  public function onError(Request $request) {
    $request_id = Html::escape($request->get('request-id'));
    $this->logger->error("eTranslation response error (@request_id). Please make sure eTranslation API is working as expected.", ['@request_id' => $request_id]);
    $this->cache()->set(EtranslationUtils::getCacheKey($request_id), EtranslationUtils::$errorValue);
    return new Response();
  }

  /**
   * Parse translation response and insert entity translation.
   *
   * @param [type] $entity
   *   Original entity to translate.
   * @param [type] $translated_content
   *   Translation received from eTranslation service.
   * @param [type] $target_language
   *   Language of translation received.
   */
  private function processEntityTranslation($entity, $translated_content, $target_language) {
    $target_langcode     = $this->getLangcodeByLanguage($target_language);
    $translation_service = $this->translationManager->translationService;
    if ($translation_service instanceof EtranslationService) {
      $xml_translations = $translation_service->etranslationResponseToTranslationArray($translated_content);

      $translatable_fields = $this->translationManager->getTranslatableFields($entity);
      $original_values     = $this->translationManager->extractTranslatableValues($entity, $translatable_fields);
      if (count($original_values) === count($xml_translations)) {
        $translations = $translation_service->decodeXmlTranslations($original_values, $xml_translations);
        $this->translationManager->translateEntity($entity->id(), $entity->getEntityTypeId(), $target_langcode, FALSE, $translations);
      }
    }
  }

  /**
   * In languages added to the website find langcode for given language.
   *
   * @param string $language
   *   Requested language e.g. 'en'.
   *
   * @return string
   *   Langcode or null if langcode not found.
   */
  private function getLangcodeByLanguage($language) {
    $languages = $this->languageManager()->getLanguages();
    $language_lowercase = strtolower($language);

    foreach ($languages as $langcode => $value) {
      if (str_starts_with($langcode, $language_lowercase)) {
        return $langcode;
      }
    }
    $this->logger->error("Could not find langcode for language '@language'. Please check if the language code of this language is correct!", ['@language' => $language_lowercase]);
    return NULL;
  }

}
