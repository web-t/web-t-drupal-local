<?php

namespace Drupal\webt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webt\AssetManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller that generates content for About section.
 */
class AboutTabController extends ControllerBase {

  /**
   * Asset manager.
   *
   * @var Drupal\webt\AssetManager
   */
  private $assetManager;

  /**
   * Constructor.
   *
   * @param Drupal\webt\AssetManager $asset_manager
   *   Asset manager.
   */
  public function __construct(AssetManager $asset_manager) {
    $this->assetManager = $asset_manager;
  }

  /**
   * Generate markup.
   *
   * @return array
   *   About tab content
   */
  public function content() {
    return [
      'about_section' => [
        '#type' => 'container',
        'about_heading' => [
          '#type' => 'html_tag',
          '#tag' => 'h5',
          '#value' => $this->t('About WEB-T module'),
        ],
        'about_content' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('WEB-T is a module that allows you to translate your Drupal website content using machine translation. It comes with a default translation provider, eTranslation, but can be configured to use custom providers.'),
        ],
        'configure_content' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('See how to <a target="_blank" href=":link">configure and use this plugin</a>.', [
            ':link' => 'https://website-translation.language-tools.ec.europa.eu/solutions/drupal_en',
          ]),
        ],
      ],
      'more_info_section' => [
        '#type' => 'container',
        'more_info_heading' => [
          '#type' => 'html_tag',
          '#tag' => 'h5',
          '#value' => $this->t('More information'),
        ],
        'more_info_content' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('WEB-T is a part of European Multilingual Web (EMW) project by EC (<a target="_blank" href=":link">Learn more</a>).', [
            ':link' => 'https://website-translation.language-tools.ec.europa.eu/web-t-connecting-languages-0_en',
          ]),
        ],
      ],
      'logos' => [
        '#type' => 'container',
        'ec_logo' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'alt' => $this->t('EC logotype'),
            'class' => ['logotypes'],
            'src' => $this->assetManager->getAssetUrl('images/ec_logotype.svg'),
          ],
        ],
        'webt_logo' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'alt' => $this->t('WEB-T logotype'),
            'class' => ['logotypes'],
            'id' => 'webt_logtype',
            'src' => $this->assetManager->getAssetUrl('images/webt_logotype.svg'),
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'webt/about_tab.style',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webt.asset_manager')
    );
  }

}
