<?php

namespace Drupal\webt;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\webt\Model\ProcessingResponse;
use Drupal\webt\Model\StringType;
use Drupal\webt\Model\TranslationStatus;

/**
 * Service that creates/updates/deletes translated versions of source items.
 */
class TranslationManager {

  use LoggerChannelTrait;

  /**
   * Machine translation service that translates strings.
   *
   * @var Drupal\webt\translation_engines\AbstractTranslationService
   */
  public $translationService;

  /**
   * Drupal config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Locale storage.
   *
   * @var \Drupal\locale\StringDatabaseStorage
   */
  protected $localeStorage;
  /**
   * Config storage.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected $configStorage;
  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Default language.
   *
   * @var \Drupal\Core\Language\LanguageDefault
   */
  protected $languageDefault;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Initializes TranslationManager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\webt\translation_engines\generic\GenericMTService $genericMtService
   *   The generic machine translation service.
   * @param \Drupal\webt\translation_engines\etranslation\EtranslationService $etranslationService
   *   The eTranslation service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\locale\StringDatabaseStorage $localeStorage
   *   The locale storage service.
   * @param \Drupal\Core\Config\CachedStorage $configStorage
   *   The config storage service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageDefault $languageDefault
   *   The default language service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    $genericMtService,
    $etranslationService,
    $languageManager,
    $localeStorage,
    $configStorage,
    $entityTypeManager,
    $languageDefault
  ) {
    $this->configFactory = $config_factory;
    $engine = $config_factory->get('webt.settings')->get('mt_engine');
    $this->translationService = $engine !== 'etranslation' ? $genericMtService : $etranslationService;
    $this->languageManager = $languageManager;
    $this->localeStorage = $localeStorage;
    $this->configStorage = $configStorage;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageDefault = $languageDefault;
    $this->logger = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
  }

  /**
   * Create translations for given entities in given target languages.
   *
   * @param array $entities
   *   Entities to be translated.
   * @param string $type
   *   Entity type needed for progress bar message updates.
   * @param string[] $target_langcodes
   *   Target language codes.
   * @param bool $skip_if_translated
   *   By default function skips already translated entities.
   *   Retranslates them if $skip_if_translated is false.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function translateEntities($entities, $type, $target_langcodes, $skip_if_translated = TRUE) {
    $translatable_values   = [];
    $translatable_enitites = [];

    $translated_versions_to_create = 0;
    $translated_versions_created   = 0;

    // Group entities by src-trg language.
    foreach ($entities as $entity) {
      $source_langcode = $entity->language()->getId();
      foreach ($target_langcodes as $langcode) {
        if ($langcode === $source_langcode) {
          continue;
        }
        if ($skip_if_translated && $entity->hasTranslation($langcode)) {
          continue;
        }
        $key = "$source_langcode->$langcode";

        if (!isset($translatable_values[$key])) {
          $translatable_values[$key]   = [];
          $translatable_enitites[$key] = [];
        }

        $translatable_fields = $this->getTranslatableFields($entity);
        if (!empty($translatable_fields)) {
          $translated_versions_to_create++;
          $entity_values                 = $this->extractTranslatableValues($entity, $translatable_fields);
          $translatable_values[$key]     = array_merge($translatable_values[$key], $entity_values);
          $translatable_enitites[$key][] = $entity;
        }
      }
    }

    // Translate entities for each src-trg language pair.
    foreach ($translatable_values as $key => $strings_to_translate) {
      $key_parts            = explode('->', $key);
      $src_lang             = $key_parts[0];
      $trg_lang             = $key_parts[1];
      $translation_response = $this->translationService->translate($src_lang, $trg_lang, $strings_to_translate, $type);

      if (empty($translation_response->translations) || TranslationStatus::FULL_RESULT !== $translation_response->status) {
        continue;
      }

      $offset = 0;
      foreach ($translatable_enitites[$key] as $entity) {
        $entity_translations        = [];
        $entity_translatable_fields = $this->getTranslatableFields($entity);
        $translatable_value_count   = count($this->extractTranslatableValues($entity, $translatable_fields));
        for ($i = 0; $i < $translatable_value_count; $i++, $offset++) {
          $entity_translations[] = $translation_response->translations[$offset];
        }
        if (!empty($entity_translations)) {
          $this->entityCreateTranslation($entity, $trg_lang, $entity_translatable_fields, $entity_translations);
          $translated_versions_created++;
        }
      }
    }

    return $this->getProcessingResponse($translated_versions_created, $translated_versions_to_create);
  }

  /**
   * Translate entity from specified source language to target language.
   *
   * @param int $entityId
   *   Entity ID
   * @param string $entityTypeId
   *   Entity type ID
   * @param string[] $trg_lang
   *   Target language code
   * @param boolean $skip_if_translation_exists
   *   Whether to skip translation if translation already exists
   * @param string[] $translations
   *   If provided, uses given string translations, skips machine translation.
   */
  public function translateEntity($entityId, $entityTypeId, $trg_lang, $skip_if_translation_exists = FALSE, $translations = NULL) {
    $entity = $this->loadEntityByIdAndType($entityId, $entityTypeId);
    if (!$entity) {
      return;
    }    
    $src_lang = $entity->language()->getId();

    if ($entity instanceof ContentEntityBase && $entity->isTranslatable() && (!$skip_if_translation_exists || !$entity->hasTranslation($trg_lang))) {
      $translatable_fields = $this->getTranslatableFields($entity);
      if (count($translatable_fields) > 0) {
        $this->logger->info("Translating entity (@src_lang-@trg_lang)", ['@src_lang' => $src_lang, '@trg_lang' => $trg_lang]);
  
        $strings_to_translate = $this->extractTranslatableValues($entity, $translatable_fields);
  
        if (!$translations) {
          $translation_response = $this->translationService->translate($src_lang, $trg_lang, $strings_to_translate, 'entity', FALSE, $entity);
          $translations         = $translation_response->translations;
          if (TranslationStatus::FULL_RESULT !== $translation_response->status) {
            return;
          }
        }
        $this->entityCreateTranslation($entity, $trg_lang, $translatable_fields, $translations);
      }
    }
  }

  /**
   * Delete translations of entities in specified languages.
   *
   * @param \Drupal\Core\Entity\Entity[] $entities
   *   Entities to delete translations of.
   * @param string[] $langcodes
   *   Language codes of translations which need to be deleted.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function deleteTranslationsOfEntities($entities, $langcodes) {
    foreach ($entities as $entity) {
      $source_langcode = $entity->language()->getId();
      foreach ($langcodes as $langcode) {
        if ($langcode === $source_langcode) {
          continue;
        }
        if ($entity->hasTranslation($langcode)) {
          $entity->removeTranslation($langcode);
          $entity->save();
        }
      }
    }
    return ProcessingResponse::$fullyProcessed;
  }

  /**
   * Retrieves translation languages for entity
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   Entity to translate.
   * @return string[]
   *   List of languages
   */
  public function getTranslationLanguagesForEntity($entity) {
    $source_langcode       = $entity->language()->getId();
    $langcodes             = $this->languageManager->getLanguages();
    $langcodes_list        = array_keys($langcodes);
    $translation_languages = [$source_langcode => []];

    foreach ($langcodes_list as $langcode) {
      if ($langcode === $source_langcode || !$this->translationService->isLanguageSupported($langcode)) {
        continue;
      }
      $translation_languages[$source_langcode][] = $langcode;
    }
    return $translation_languages;
  }

  /**
   * Translate given UI strings and save translations.
   *
   * @param \Drupal\locale\TranslationString[] $translatable_strings
   *   Translatable UI strings.
   * @param string $src_lang
   *   Source language of UI strings.
   * @param string $trg_lang
   *   Target language.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function translateUiStrings($translatable_strings, $src_lang, $trg_lang) {
    $values               = array_map(
    function ($s) {
      return $s->source;
    },
            $translatable_strings
    );
    $translation_response = $this->translationService->translate($src_lang, $trg_lang, $values, StringType::UI, TRUE);
    $translations         = $translation_response->translations;
    $count                = count($translations);

    for ($i = 0; $i < $count; $i++) {
      $translated_string = $translations[$i];
      $translation       = $this->localeStorage->createTranslation(
      [
        'lid'         => $translatable_strings[$i]->lid,
        'language'    => $trg_lang,
        'translation' => $translated_string,
      ]
      );
      $translation->save();
    }
    return $this->getProcessingResponse($count, count($values));
  }

  /**
   * Get list of all untranslated UI strings for a language.
   *
   * @param string $langcode
   *   Translation language code.
   *
   * @return \Drupal\locale\TranslationString[]
   *   Untranslated UI strings
   */
  public function getUntranslatedUiStrings($langcode) {
    $existing_translations = $this->localeStorage->getTranslations(['language' => $langcode]);

    $untranslated_strings = array_filter(
    $existing_translations,
    function ($s) {
      $has_source      = isset($s->source) && !empty($s->source);
      $has_translation = isset($s->translation);
      return $has_source && !$has_translation;
    }
    );
    return array_values($untranslated_strings);
  }

  /**
   * Delete all UI translations for given languages.
   *
   * @param string[] $langcodes
   *   Language codes to delete translations of.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function deleteUiTranslations($langcodes) {
    $source_langcode = $this->getDefaultLanguageCode();

    foreach ($langcodes as $langcode) {
      if ($langcode === $source_langcode) {
        continue;
      }
      $this->localeStorage->deleteTranslations(['language' => $langcode]);
    }
    return ProcessingResponse::$fullyProcessed;
  }

  /**
   * Translate given configuration strings to a language.
   *
   * @param array $config_strings
   *   Translatable configuration string items.
   * @param string $langcode
   *   Language code to translate to.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function translateConfigStrings($config_strings, $langcode) {
    // Prepare source value list for translation.
    $source_strings = [];
    foreach ($config_strings as $item) {
      array_walk_recursive(
      $item,
      function ($p) use (&$source_strings) {
        if (is_string($p)) {
          $source_strings[] = $p;
        }
      }
      );
    }
    $offset = 0;
    if (!empty($source_strings)) {
      // translate.
      $source_langcode      = $this->getDefaultLanguageCode();
      $translation_response = $this->translationService->translate($source_langcode, $langcode, $source_strings, StringType::CONFIG, TRUE);
      $translations         = $translation_response->translations;

      // Replace source values with translations & save.
      if (!empty($translations) && TranslationStatus::FULL_RESULT === $translation_response->status) {
        $collection = $this->configStorage->createCollection("language.$langcode");
        foreach ($config_strings as $key => $value) {
          $translated_data = $value;
          array_walk_recursive(
          $translated_data,
          function (&$v) use ($translations, &$offset) {
            if (is_string($v)) {
              $v = $translations[$offset];
              $offset++;
            }
          }
          );
          $collection->write($key, $translated_data);
        }
      }
    }

    return $this->getProcessingResponse($offset, count($source_strings));
  }

  /**
   * Gets all untranslated cofiguration strings for a language.
   *
   * @param string $langcode
   *   Language code.
   *
   * @return array
   *   Untranslated configuration strings
   */
  public function getUntranslatedConfigStrings($langcode) {
    $translatable_items    = $this->getTranslatableConfigStrings();
    $translated_item_names = $this->configStorage->createCollection("language.$langcode")->listAll();

    // Get untranslated config item names.
    $missing_items = array_filter(
    $translatable_items,
    function ($name) use ($translated_item_names) {
      return !in_array($name, $translated_item_names, TRUE);
    },
            ARRAY_FILTER_USE_KEY
    );

    return $missing_items;
  }

  /**
   * Get all translatable configuration strings (in source language).
   *
   * @return array
   *   Translatable config strings
   */
  private function getTranslatableConfigStrings() {
    $source_item_names  = $this->configFactory->listAll();
    $config_items       = $this->configFactory->loadMultiple($source_item_names);
    $translatable_items = [];

    // Identify translatable configuration items and their fields.
    foreach ($config_items as $item) {
      $original = $item->getOriginal();
      if (isset($original['langcode'])) {
        $translatable_fields = $this->getTranslatableConfigFields($original);
        if (!empty($translatable_fields)) {
          $translatable_items[$item->getName()] = $translatable_fields;
        }
      }
    }

    return $translatable_items;
  }

  /**
   * Get translatable properties & their values for a configuration string item.
   *
   * @param mixed $config_item
   *   Configuration string item.
   *
   * @return array
   *   Translatable config fields
   */
  private function getTranslatableConfigFields($config_item) {
    $translatable_fields = [];
    // Keys that usually hold translatable values.
    $search_keys = ['label', 'name', 'description', 'message'];
    // Arrays that can contain $search_keys.
    $container_keys = ['settings'];

    foreach ($search_keys as $key) {
      // Check 1st level values.
      if (isset($config_item[$key]) && strlen($config_item[$key]) > 0) {
        if ('name' === $key && in_array('label', array_keys($translatable_fields), TRUE)) {
          // Do not translate 'name' if 'label' already present.
          // Workaround for drupal/admin/config/media/image-styles translation.
          continue;
        }
        $translatable_fields[$key] = $config_item[$key];
      }
      // Check 2nd level values.
      foreach ($container_keys as $container_key) {
        if (isset($config_item[$container_key]) && isset($config_item[$container_key][$key]) &&
        strlen($config_item[$container_key][$key]) > 0) {
          if (!isset($translatable_fields[$container_key])) {
            $translatable_fields[$container_key] = [];
          }
          $translatable_fields[$container_key][$key] = $config_item[$container_key][$key];
        }
      }
    }

    return $translatable_fields;
  }

  /**
   * Delete all configuration string translations for given languages.
   *
   * @param string[] $langcodes
   *   Target language codes.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  public function deleteConfigTranslations($langcodes) {
    $source_langcode = $this->getDefaultLanguageCode();

    foreach ($langcodes as $langcode) {
      if ($langcode === $source_langcode) {
        continue;
      }
      $this->configStorage->createCollection("language.$langcode")->deleteAll();
    }
    return ProcessingResponse::$fullyProcessed;
  }

  /**
   * Get translatable fields for given entity.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   Entity.
   *
   * @return array
   *   Translatable entity fields
   */
  public function getTranslatableFields($entity) {
    switch ($entity->getEntityTypeId()) {
      case 'node':
        return [
          'title'       => ['value'],
          'body'        => ['value', 'summary'],
          'field_image' => ['alt'],
        ];

      case 'comment':
        return [
          'subject'      => ['value'],
          'comment_body' => ['value'],
        ];

      case 'taxonomy_term':
        return ['name' => ['value']];

      default:
        // Type not implemented.
        return [];
    }
  }

  /**
   * Checks if entity needs retranslation.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   Entity.
   *
   * @return bool
   *   True if translatable content of entity has changed. False otherwise
   */
  public function entityTranslatableContentChanged($entity) {
    $translatable_fields = $this->getTranslatableFields($entity);

    if (count($translatable_fields) > 0) {
      foreach ($translatable_fields as $parent => $fields) {
        foreach ($fields as $field) {
          if (!$entity->original || ($entity->$parent && $entity->$parent->$field !== $entity->original->$parent->$field)) {
            return TRUE;
          }
        }
      }
    }

    if ($entity->hasField('field_tags') && $entity->field_tags->referencedEntities() !== $entity->original->field_tags->referencedEntities()) {
      return TRUE;
    }
    if ($entity->hasField('field_image') && $entity->field_image->entity !== $entity->original->field_image->entity) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get translation progress percentage for given content type and language.
   *
   * @param string $type
   *   Content type.
   * @param string $langcode
   *   Target language code.
   *
   * @return float
   *   Translation progress in percents
   */
  public function getTranslationProgressPercents($type, $langcode) {
    $source_language  = $this->getDefaultLanguageCode();
    $source_count     = 0;
    $translated_count = 0;
    switch ($type) {
      case StringType::UI:
        if ($source_language === $langcode) {
          return 100;
        }
        $source_count     = count($this->localeStorage->getStrings());
        $translated_count = $source_count - count($this->getUntranslatedUiStrings($langcode));
        break;

      case StringType::CONFIG:
        if ($source_language === $langcode) {
          return 100;
        }
        else {
          $source_strings   = $this->getTranslatableConfigStrings();
          $source_count     = count($source_strings);
          $translated_count = $source_count - count($this->getUntranslatedConfigStrings($langcode));
          break;
        }
      default:
        $source_strings     = $this->entityTypeManager
          ->getStorage($type)
          ->loadMultiple();
        $translated_strings = array_filter(
        $source_strings,
        function ($e) use ($langcode) {
          return $e->language()->getId() === $langcode || $e->hasTranslation($langcode);
        }
        );
        $source_count       = count($source_strings);
        $translated_count   = count($translated_strings);
        break;
    }
    return $source_count > 0 ? (float) 100 * $translated_count / $source_count : 0;
  }

  /**
   * Get default language code of the website.
   *
   * @return string
   *   Default langcode
   */
  public function getDefaultLanguageCode() {
    return $this->languageDefault->get()->getId();
  }

  /**
   * Create translated version of an entity.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   Entity to translate.
   * @param string $trg_lang
   *   Target language code.
   * @param array $translation_fields
   *   Translatable fields for this entity.
   * @param array $translation_values
   *   Translated values of translatable fields.
   */
  private function entityCreateTranslation($entity, $trg_lang, $translation_fields, $translation_values) {
    $translation_count  = count($translation_values);
    $source_value_count = count($this->extractTranslatableValues($entity, $translation_fields));
    if (count($translation_fields) > 0 && $source_value_count === $translation_count && $entity->isTranslatable()) {
      if ($entity->hasTranslation($trg_lang)) {
        $entity->removeTranslation($trg_lang);
      }

      $field_translations = [];
      $offset             = 0;
      foreach ($translation_fields as $parent => $fields) {
        $field_translations[$parent] = [];
        foreach ($fields as $field) {
          $field_translations[$parent][$field] = $translation_values[$offset];
          $offset++;
        }
      }

      $translation = $entity->addTranslation($trg_lang, $field_translations);

      $body_key = NULL;
      switch ($entity->getEntityTypeId()) {
        case 'node':
          $body_key = 'body';
          break;

        case 'comment':
          $body_key = 'comment_body';
          break;
      }

      if ($body_key) {
        $translation->$body_key->format = $entity->$body_key->format;
      }

      if ($entity->hasField('field_tags')) {
        $translation->field_tags = $entity->field_tags;
      }

      if ($entity->hasField('field_image')) {
        $translation->set(
        'field_image',
        [
          'target_id' => $entity->field_image->target_id,
          'alt'       => $translation->field_image->alt,
        ]
        );
      }

      if ($entity->hasField('status')) {
        $translation->status->value = $entity->status->value;
      }

      if ($entity->hasField('content_translation_source')) {
        $translation->content_translation_source->value = $entity->langcode->value;
      }

      $translation->save();
    }
  }

  /**
   * Get processing response status based on translation and reference count.
   *
   * @param int $translation_count
   *   Translated string value count.
   * @param int $reference_value_count
   *   Original (reference) string value count.
   *
   * @return int
   *   Value of ProcessingResponse
   */
  private function getProcessingResponse($translation_count, $reference_value_count) {
    if (0 === $reference_value_count) {
      return ProcessingResponse::$alreadyProcessed;
    }
    elseif ($translation_count === $reference_value_count) {
      return ProcessingResponse::$fullyProcessed;
    }
    elseif (0 === $translation_count && 0 < $reference_value_count) {
      return ProcessingResponse::$processingError;
    }
    else {
      return ProcessingResponse::$partiallyProcessed;
    }
  }

  /**
   * Get translatable values from an entity using list of translatable fields.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   Entity.
   * @param array $translatable_fields
   *   Translatable fields of given entity.
   *
   * @return string[]
   *   Translatable values
   */
  public function extractTranslatableValues($entity, $translatable_fields) {
    $translatable_values = [];
    foreach ($translatable_fields as $parent => $fields) {
      foreach ($fields as $field) {
        $translatable_values[] = $entity->$parent->$field ?? '';
      }
    }
    return $translatable_values;
  }

  /**
   * Retrieve entity by ID.
   *
   * @param [type] $id
   *   Identifier of entity.
   * @param [type] $type
   *   Type of entity.
   *
   * @return \Drupal\Core\Entity\Entity
   *   Entity or null if enitity was not found
   */
  public function loadEntityByIdAndType($id, $type) {
    $storage = $this->entityTypeManager->getStorage($type);
    $entity  = $storage->load($id);
    if ($entity) {
      return $entity;
    }
    else {
      $this->logger->error("Could not load entity by ID (@entity_id). Please make sure such entity exists!", ['@entity_id' => $id]);
      return NULL;
    }
  }
}
